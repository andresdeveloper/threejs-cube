## Slimopgewekt's Educube gemaakt met ThreeJS

3D cube that rotates, and depending on wich side is facing upwards changing the lighting of the room.

You can use the [editor on GitHub](https://github.com/vaporlain/Threejs-Cube/edit/master/README.md) to maintain and preview the content for your website in Markdown files.

Whenever you commit to this repository, GitHub Pages will run [Jekyll](https://jekyllrb.com/) to rebuild the pages in your site, from the content in your Markdown files.

# Educube handleiding
## Gemaakt door Andrés Bechger op donderdag 1 februari 
 
Dit zijn korte instructies voor het onderhouden van de 3D Educube op Wordpress, gericht op (toekomstige) collega’s. 
 
## Locatie
Er wordt hier niet verteld wat de gegevens zijn om in te loggen.
De 3D Educube is geïmplementeerd via wordpress. Dit is het domein http://staging.educube.nl/ .
Om naar de wordpress te gaan gebruik je de link: http://staging.educube.nl/wp-admin/ 
Het zit in een html iframe, aangesproken via een stukje code in de pagina. Daar staat een link naar de 3D Educube als bijlage die en andere afbeeldingen zitten onder het kopje media. 

## Aanpassen
A.	Hoe pas je het aspect van de 3D kubus aan?
Elke zijde, achtergrond en vloer heeft een plaatje. Dit is een bestand die staat bij media. Om deze te vervangen/aan te passen moet je het bestaande plaatje weghalen. Daarna eentje uploaden met dezelfde naam, en hetzelfde bestands type.

B.	Hoe pas je de code aan van de Educube?
Download de bijlage index.html en pas die aan met een code editor. Daarna sla je die op, upload je het als een media/ bijlage, en controleert of de link nog steeds overeenkomt met die in de pagina. 

## Onderhoud
Na een wordpress update kan alles verandert zijn. Niet zo lang geleden waren alle links verandert door een update. Hier haal ik uit dat als de persoon die verantwoordelijk is hiervoor geen kennis heeft van programmeren, hij/zij beter updates uitzet. 
